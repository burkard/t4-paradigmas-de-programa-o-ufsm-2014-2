-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 07/12/2014 às 21:10
-- Versão do servidor: 5.5.40-0ubuntu0.14.04.1
-- Versão do PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `paradigmas-t4`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `endereco` varchar(45) NOT NULL,
  `telefone` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Fazendo dump de dados para tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `endereco`, `telefone`) VALUES
(1, 'Marcelo F. Burkard', 'Rua Mal. Floriano Peixoto, 1367/405', '(55) 9606-2389'),
(4, 'Fernanda Didonet de Assis44', 'Coração 55', '(55) 9932-2389');

-- --------------------------------------------------------

--
-- Estrutura para tabela `compra`
--

CREATE TABLE IF NOT EXISTS `compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valor_total` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Fazendo dump de dados para tabela `compra`
--

INSERT INTO `compra` (`id`, `valor_total`) VALUES
(9, 5.98),
(10, 2),
(11, 3.49),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 2.49),
(17, 112.05000000000001),
(18, 2.98),
(19, 150.05);

-- --------------------------------------------------------

--
-- Estrutura para tabela `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `valor` double NOT NULL,
  `estoque` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Fazendo dump de dados para tabela `item`
--

INSERT INTO `item` (`id`, `nome`, `valor`, `estoque`) VALUES
(1, 'Coca-cola 2L', 2.49, 53),
(2, 'Água mineral com gás', 1, 2),
(3, 'Água mineral sem gás', 1.2, 0),
(4, 'Guaraná Antactica 3,3L', 3.59, 0),
(5, 'ab', 2.45, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `operacao_has_item`
--

CREATE TABLE IF NOT EXISTS `operacao_has_item` (
  `operacao_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  PRIMARY KEY (`operacao_id`,`item_id`),
  KEY `fk_operacao_has_item_item1_idx` (`item_id`),
  KEY `fk_operacao_has_item_operacao_idx` (`operacao_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `operacao_has_item`
--

INSERT INTO `operacao_has_item` (`operacao_id`, `item_id`, `quantidade`) VALUES
(9, 1, 2),
(9, 3, 1),
(10, 1, 2),
(10, 3, 1),
(11, 1, 1),
(11, 2, 1),
(16, 1, 1),
(17, 1, 45),
(18, 1, 2),
(19, 1, 100);

-- --------------------------------------------------------

--
-- Estrutura para tabela `venda`
--

CREATE TABLE IF NOT EXISTS `venda` (
  `operacao_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  KEY `fk_venda_cliente1_idx` (`cliente_id`),
  KEY `fk_venda_operacao1_idx` (`operacao_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `venda`
--

INSERT INTO `venda` (`operacao_id`, `cliente_id`) VALUES
(11, 1),
(12, 1),
(13, 4),
(16, 1),
(17, 1),
(18, 4);

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `operacao_has_item`
--
ALTER TABLE `operacao_has_item`
  ADD CONSTRAINT `fk_operacao_has_item_operacao` FOREIGN KEY (`operacao_id`) REFERENCES `compra` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_operacao_has_item_item1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `venda`
--
ALTER TABLE `venda`
  ADD CONSTRAINT `fk_venda_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_venda_operacao1` FOREIGN KEY (`operacao_id`) REFERENCES `compra` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
