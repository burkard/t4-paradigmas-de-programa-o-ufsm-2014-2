/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import helper.ConexaoBD;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author marcelo
 */
public abstract class DAO {
    Connection con;
    PreparedStatement prSt;
    ResultSet res;
    private String tabela;
    public String getSelectQuery() {
        return "SELECT * FROM " + this.getTabela();
    }
    public abstract ArrayList select();
    public abstract Object findOne(int id);
    public String getFindOneQuery() {
        return "SELECT * FROM " + this.getTabela() + " WHERE id=?";
    }
    public String getDeleteQuery() {
        return "DELETE FROM " + this.getTabela() + " WHERE id=?";
    }
    public void delete(int id) {
        try {
            this.con = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getDeleteQuery());
            this.prSt.setInt(1, id);
            this.prSt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
    }
    public abstract String getInsertQuery();
    public String getLastInsertedIdQuery() {
        return "SELECT id FROM " + this.getTabela() + " ORDER BY id DESC LIMIT 1";
    }
    public int getLastInsertedId() throws SQLException {
        int id = 0;
        try {
            this.setCon(ConexaoBD.conectar());
            this.setPrSt(this.getCon().prepareStatement(this.getLastInsertedIdQuery()));
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                id = this.res.getInt("id");
            }
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.getCon(), this.getPrSt(), this.getRes());
        }
        return id;
    }
    public String getUpdateQuery() {
        return "UPDATE " + this.getTabela();
    }
    public Connection getCon() {
        return con;
    }
    public void setCon(Connection con) {
        this.con = con;
    }
    public PreparedStatement getPrSt() {
        return prSt;
    }
    public void setPrSt(PreparedStatement prSt) {
        this.prSt = prSt;
    }
    public ResultSet getRes() {
        return res;
    }
    public void setRes(ResultSet res) {
        this.res = res;
    }
    public String getTabela() {
        return tabela;
    }
    public void setTabela(String tabela) {
        this.tabela = tabela;
    }
}
