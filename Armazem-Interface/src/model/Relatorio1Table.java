/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author marcelo
 */
public class Relatorio1Table extends AbstractTable {
    
    private static final String[] columnNames = {"Nome", "Valor", "Estoque"};

    private final ArrayList<Item> records;
    
    public Relatorio1Table() {
        ItemDAO itemDAO = new ItemDAO();
        records = itemDAO.selectEstoque();
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public String getColumnName(int index) {
        return columnNames[index];
    }        

    public String getColumnCount(int columnIndex) {
        return columnNames[columnIndex];
    }    
    
    @Override
    public int getRowCount() {
        return records.size();
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex) {
            case 0: return records.get(rowIndex).getNome();
            case 1: return records.get(rowIndex).getValor();
            case 2: return records.get(rowIndex).getEstoque();
        }
        return null;
    }    
    
}
