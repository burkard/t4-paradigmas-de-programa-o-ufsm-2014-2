/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import helper.ConexaoBD;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author marcelo
 */
public abstract class OperacaoDAO extends DAO {
    
    public ArrayList<ItemOperacao> selectItensOperacao(int compraId) {
        ArrayList<ItemOperacao> itensCompra = new ArrayList<>();
        try {
            ItemDAO itemDAO = new ItemDAO();
            ResultSet resLocal;
            Compra compraAux;
            PreparedStatement prStAux = this.con.prepareStatement(this.getSelectItensOperacaoQuery());
            prStAux.setInt(1, compraId);
            resLocal = prStAux.executeQuery();
            while (resLocal.next()) {
                itensCompra.add(new ItemOperacao(itemDAO.findOne(resLocal.getInt("item_id")), resLocal.getInt("quantidade")));
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        }
        return itensCompra;
    }
    
    public String getInsertItensOperacaoQuery() {
        return "INSERT INTO operacao_has_item (operacao_id, item_id, quantidade) VALUES (?, ?, ?)";
    }
    
    public String getSelectItensOperacaoQuery() {
        return "SELECT * FROM operacao_has_item WHERE operacao_id=?";
    }
    
    public void insertItensOperacao(int OperacaoId, ArrayList<ItemOperacao> itens) {
        try {
            this.setCon(ConexaoBD.conectar());
            for (ItemOperacao iten : itens) {
                this.setPrSt(this.getCon().prepareStatement(this.getInsertItensOperacaoQuery()));
                this.getPrSt().setInt(1, OperacaoId);
                this.getPrSt().setInt(2, iten.getItem().getId());
                this.getPrSt().setInt(3, iten.getQuantidade());
                this.getPrSt().executeUpdate();
            }
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.getCon(), this.getPrSt(), this.getRes());
        }
    }
    
}
