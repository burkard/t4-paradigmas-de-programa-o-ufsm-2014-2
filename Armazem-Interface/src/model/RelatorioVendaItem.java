/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author marcelo
 */
public class RelatorioVendaItem {
    private Item item;
    private int quantidade;

    public RelatorioVendaItem(Item item, int quantidade) {
        this.item = item;
        this.quantidade = quantidade;
    }

    public RelatorioVendaItem() {
        
    }
    
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
}
