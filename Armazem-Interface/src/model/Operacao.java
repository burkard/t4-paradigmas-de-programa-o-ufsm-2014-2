/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author marcelo
 */
abstract public class Operacao extends Registro {
    public ArrayList<ItemOperacao> itens = new ArrayList<>();
    double valorTotal;
    abstract void atualizarEstoque();
    public ArrayList<ItemOperacao> getItens() {
        return itens;
    }
    public void setItens(ArrayList<ItemOperacao> itens) {
        this.itens = itens;
    }
    public double getValorTotal() {
        return valorTotal;
    }
    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }    
}
