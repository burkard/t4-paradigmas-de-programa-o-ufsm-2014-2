/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author marcelo
 */
public class ItemTable extends AbstractTable {
    
    private static final String[] columnNames = {"#", "Nome", "Valor", "Estoque"};

    private final ArrayList<Item> records;
    
    public ItemTable() {
        ItemDAO itemDAO = new ItemDAO();
        records = itemDAO.select();
    }
    
    public void remove(int index) {
        // Remove do BD
        ItemDAO itemDAO = new ItemDAO();
        Item item = this.select(index);
        itemDAO.delete(item.getId());
        // Remove da View
	records.remove(index);
	fireTableRowsDeleted(index, index);
    }
    
    public Item select(int index) {
        return records.get(index);
    }
    
    public void add(Item item) throws SQLException {
        // Adiciona um elemento no BD
        ItemDAO itemDAO = new ItemDAO();
        itemDAO.insert(item);
        // Atribui o ID do auto-incremento do bd ao objeto
        item.setId(itemDAO.getLastInsertedId());
        // Adiciona um elemento na ultima posição da lista
        records.add(item);
        fireTableRowsInserted(records.size()-1, records.size()-1);
    }
    
    public void update(int index, Item item) {
        // Altera entidade no BD
        ItemDAO itemDAO = new ItemDAO();
        Item itemAux = this.select(index);
        itemDAO.update(itemAux);
        item.setId(itemAux.getId());
        // Altera elemento na view
        records.set(index, item);
        fireTableRowsUpdated(index, index);
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public String getColumnName(int index) {
        return columnNames[index];
    }        

    public String getColumnCount(int columnIndex) {
        return columnNames[columnIndex];
    }    
    
    @Override
    public int getRowCount() {
        return records.size();
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex) {
            case 0: return records.get(rowIndex).getId();
            case 1: return records.get(rowIndex).getNome();
            case 2: return records.get(rowIndex).getValor();
            case 3: return records.get(rowIndex).getEstoque();
        }
        return null;
    }
    
}
