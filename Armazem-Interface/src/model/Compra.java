/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author marcelo
 */
public final class Compra extends Operacao {
    public Compra(ArrayList<ItemOperacao> itens, double valorTotal) {
        this.setItens(itens);
        this.setValorTotal(valorTotal);
        this.atualizarEstoque();
    }

    public Compra() {
        
    }

    void atualizarEstoque() {
        ItemDAO itemDAO = new ItemDAO();
        for (ItemOperacao iten : this.getItens()) {
            Item item = itemDAO.findOne(iten.getItem().getId());
            item.setEstoque(item.getEstoque() + iten.getQuantidade());
            itemDAO.update(item);
        }
    }
    
}
