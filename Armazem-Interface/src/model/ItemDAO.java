/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import helper.ConexaoBD;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author marcelo
 */
public class ItemDAO extends DAO {
    
    public ItemDAO() {
        this.setTabela("item");
    }
    
    @Override
    public ArrayList select() {
        ArrayList<Item> itens = new ArrayList();
        Item item = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectQuery());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                item = new Item();
                item.setId(this.res.getInt("id"));
                item.setNome(this.res.getString("nome"));
                item.setValor(this.res.getDouble("valor"));
                item.setEstoque(this.res.getInt("estoque"));
                itens.add(item);
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return itens;
    }
    
    public String getSelectEstoqueQuery() {
        return "SELECT * FROM " + this.getTabela() + " WHERE estoque > 0";
    }
    
    public ArrayList selectEstoque() {
        ArrayList<Item> itens = new ArrayList();
        Item item = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectEstoqueQuery());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                item = new Item();
                item.setId(this.res.getInt("id"));
                item.setNome(this.res.getString("nome"));
                item.setValor(this.res.getDouble("valor"));
                item.setEstoque(this.res.getInt("estoque"));
                itens.add(item);
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return itens;
    }    
    
    public Item findOne(int id) {
        Item item = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getFindOneQuery());
            this.prSt.setInt(1, id);
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                item = new Item();
                item.setId(this.res.getInt("id"));
                item.setNome(this.res.getString("nome"));
                item.setValor(this.res.getDouble("valor"));
                item.setEstoque(this.res.getInt("estoque"));
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return item;        
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + this.getTabela() + " (nome, valor, estoque) VALUE (?, ?, ?)";
    }

    public final void insert(Item item) {
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getInsertQuery());
            this.prSt.setString(1, item.getNome());
            this.prSt.setDouble(2, item.getValor());
            this.prSt.setInt(3, item.getEstoque());
            this.prSt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE " + this.getTabela() + " SET nome=?, valor=?, estoque=? WHERE id=?";
    }

    public final void update(Item item) {
        try {
            this.con=ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getUpdateQuery());
            this.prSt.setString(1, item.getNome());
            this.prSt.setDouble(2, item.getValor());
            this.prSt.setInt(3, item.getEstoque());
            this.prSt.setInt(4, item.getId());
            this.prSt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }        
    }
    
    public String getSelectVendasItem() {
        return "SELECT item_id, SUM(quantidade) AS quantidade FROM operacao_has_item GROUP BY item_id";
    }
    
    public ArrayList selectVendasItem() {
        ArrayList<RelatorioVendaItem> itens = new ArrayList();
        RelatorioVendaItem rvi = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectVendasItem());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                ItemDAO itemDAO = new ItemDAO();
                Item item = itemDAO.findOne(this.res.getInt("item_id"));
                
                rvi = new RelatorioVendaItem();
                rvi.setQuantidade(this.res.getInt("quantidade"));
                rvi.setItem(item);
                itens.add(rvi);
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return itens;
    }
    
    public String getSelectVendasCliente() {
        return "SELECT v.cliente_id AS cliente_id, SUM(o.quantidade) AS quantidade FROM venda AS v\n" +
               "INNER JOIN operacao_has_item AS o ON (v.operacao_id = o.operacao_id)\n" +
               "GROUP BY v.cliente_id";
    }
    
    public ArrayList selectVendasCliente() {
        ArrayList<RelatorioVendaCliente> itens = new ArrayList();
        RelatorioVendaCliente rvi = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectVendasCliente());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                ClienteDAO clienteDAO = new ClienteDAO();
                Cliente cliente = clienteDAO.findOne(this.res.getInt("cliente_id"));
                
                rvi = new RelatorioVendaCliente();
                rvi.setQuantidade(this.res.getInt("quantidade"));
                rvi.setCliente(cliente);
                itens.add(rvi);
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return itens;
    }    
    
    public String getSumVendasCliente() {
            return "SELECT SUM(o.quantidade) AS quantidade FROM venda AS v\n" +
                   "INNER JOIN operacao_has_item AS o ON (v.operacao_id = o.operacao_id)";
    }
    
    public int sumVendasCliente() {
        int sum = 0;
        RelatorioVendaCliente rvi = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectVendasCliente());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                sum += this.res.getInt("quantidade");
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return sum;
    }
    
    public String getSelectVendasClienteTop5() {
        return this.getSelectVendasCliente() + " ORDER BY quantidade DESC LIMIT 5";
    }
    
    public ArrayList selectVendasClienteTop5() {
        ArrayList<RelatorioVendaCliente> itens = new ArrayList();
        RelatorioVendaCliente rvi = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectVendasClienteTop5());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                ClienteDAO clienteDAO = new ClienteDAO();
                Cliente cliente = clienteDAO.findOne(this.res.getInt("cliente_id"));
                
                rvi = new RelatorioVendaCliente();
                rvi.setQuantidade(this.res.getInt("quantidade"));
                rvi.setCliente(cliente);
                itens.add(rvi);
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return itens;
    }
    
    public String getSelectTotalSaidas() {
        return "SELECT TRUNCATE(SUM(valor_total), 2) AS total_saida FROM compra AS c WHERE c.id NOT IN (SELECT v.operacao_id FROM venda AS v)";
    }
    
    public double getTotalSaidas() {
        double totalSaidas = 0;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectTotalSaidas());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                totalSaidas = this.res.getDouble("total_saida");
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return totalSaidas;
    }
    
    public String getSelectTotalEntradas() {
        return "SELECT TRUNCATE(SUM(valor_total), 2) AS total_entrada FROM compra AS c WHERE c.id IN (SELECT v.operacao_id FROM venda AS v)";
    }
    
    public double getTotalEntradas() {
        double totalEntradas = 0;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectTotalEntradas());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                totalEntradas = this.res.getDouble("total_entrada");
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return totalEntradas;
    }
    
    public double getSaldo() {
        return this.getTotalEntradas() - this.getTotalSaidas();
    }
    
}
