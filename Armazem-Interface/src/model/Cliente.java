/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author marcelo
 */
public final class Cliente extends Registro {
    private String nome;
    private String endereco;
    private String telefone;
    public Cliente (int id, String nome, String endereco, String telefone)
    {
        this.setId(id);
        this.setNome(nome);
        this.setEndereco(endereco);
        this.setTelefone(telefone);
    }
    public Cliente() {
        
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getEndereco() {
        return endereco;
    }
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    public String getTelefone() {
        return telefone;
    }
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
