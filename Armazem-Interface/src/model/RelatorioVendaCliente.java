/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author marcelo
 */
public class RelatorioVendaCliente {
    private Cliente cliente;
    private int quantidade;

    public RelatorioVendaCliente() {
        
    }

    public RelatorioVendaCliente(Cliente cliente, int quantidade) {
        this.cliente = cliente;
        this.quantidade = quantidade;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
}
