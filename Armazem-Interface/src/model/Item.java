/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author marcelo
 */
public final class Item extends Registro {
    private String nome;
    private double valor;
    private int estoque;
    public Item(int id, String nome, double valor, int estoque)
    {
        this.setId(id);
        this.setNome(nome);
        this.setValor(valor);
        this.setEstoque(estoque);
    }

    public Item() {
        this.setEstoque(0);
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public double getValor() {
        return valor;
    }
    public void setValor(double valor) {
        this.valor = valor;
    }
    public int getEstoque() {
        return estoque;
    }
    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }
}
