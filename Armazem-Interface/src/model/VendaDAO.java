/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import helper.ConexaoBD;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author marcelo
 */
public class VendaDAO extends OperacaoDAO {
    
    public VendaDAO() {
        this.setTabela("venda");
    }

    @Override
    public ArrayList select() {
        ArrayList<Venda> vendas = new ArrayList();
        Venda venda = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectQuery());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                venda = new Venda();
                venda.setId(this.res.getInt("operacao_id"));
                venda.setOperacaoId(this.res.getInt("operacao_id"));
                venda.setClienteId(this.res.getInt("cliente_id"));
                venda.setValorTotal(this.res.getDouble("valor_total"));
                // Carrega os itens da compra
                venda.setItens(this.selectItensOperacao(venda.getId()));
                vendas.add(venda);
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return vendas;
    }
    
    public Venda findOne(int id) {
        Venda venda = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getFindOneQuery());
            this.getPrSt().setInt(1, id);
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                venda = new Venda();
                venda.setId(this.res.getInt("operacao_id"));
                venda.setOperacaoId(this.res.getInt("operacao_id"));
                venda.setClienteId(this.res.getInt("cliente_id"));
                venda.setValorTotal(this.res.getDouble("valor_total"));
                // Carrega os itens da compra
                venda.setItens(this.selectItensOperacao(venda.getId()));
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return venda;
    }    

    /**
     *
     * @return String
     */
    @Override
    public String getSelectQuery() {
        return "SELECT * FROM " + this.getTabela() + " AS v"
             + " INNER JOIN compra AS c ON (v.operacao_id = c.id)";
    }
    @Override
    public String getFindOneQuery() {
        return "SELECT * FROM " + this.getTabela() + " AS v"
             + " INNER JOIN compra AS c ON (v.operacao_id = c.id)"
             + " WHERE c.id=?";
    }
    
    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + this.getTabela() + " (operacao_id, cliente_id) VALUE (?, ?)";
    }
    
    public void insert(Venda venda) throws SQLException {
        Compra operacao = new Compra();
        operacao.setValorTotal(venda.getValorTotal());
        CompraDAO operacaoDAO = new CompraDAO();
        operacaoDAO.insert(operacao);
        int operacaoId = operacaoDAO.getLastInsertedId();
        try {
            this.setCon(ConexaoBD.conectar());
            this.setPrSt(this.getCon().prepareStatement(this.getInsertQuery()));
            this.getPrSt().setInt(1, operacaoId);
            this.getPrSt().setInt(2, venda.getClienteId());
            this.getPrSt().executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.getCon(), this.getPrSt(), this.getRes());
        }
        // Inserir itens da compra
        if (operacaoId != 0) {
            this.insertItensOperacao(operacaoId, venda.getItens());
        }
    }
   
}
