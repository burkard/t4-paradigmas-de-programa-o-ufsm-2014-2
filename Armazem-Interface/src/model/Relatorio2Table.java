/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author marcelo
 */
public class Relatorio2Table extends AbstractTable {
    
    private static final String[] columnNames = {"Produto", "Quantidade vendida"};

    private final ArrayList<RelatorioVendaItem> records;
    
    public Relatorio2Table() {
        ItemDAO itemDAO = new ItemDAO();
        records = itemDAO.selectVendasItem();
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public String getColumnName(int index) {
        return columnNames[index];
    }        

    public String getColumnCount(int columnIndex) {
        return columnNames[columnIndex];
    }    
    
    @Override
    public int getRowCount() {
        return records.size();
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex) {
            case 0: return records.get(rowIndex).getItem().getNome();
            case 1: return records.get(rowIndex).getQuantidade();
        }
        return null;
    }    
    
}
