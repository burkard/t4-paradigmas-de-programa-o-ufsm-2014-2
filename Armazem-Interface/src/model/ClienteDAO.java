/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import helper.ConexaoBD;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author marcelo
 */
public class ClienteDAO extends DAO {

    public ClienteDAO() {
        this.setTabela("cliente");
    }
    
    @Override
    public ArrayList select() {
        ArrayList<Cliente> clientes = new ArrayList();
        Cliente cliente = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectQuery());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                cliente = new Cliente();
                cliente.setId(this.res.getInt("id"));
                cliente.setNome(this.res.getString("nome"));
                cliente.setEndereco(this.res.getString("endereco"));
                cliente.setTelefone(this.res.getString("telefone"));
                clientes.add(cliente);
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return clientes;
    }
    
    public Cliente findOne(int id) {
        Cliente cliente = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getFindOneQuery());
            this.prSt.setInt(1, id);
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                cliente = new Cliente();
                cliente.setId(this.res.getInt("id"));
                cliente.setNome(this.res.getString("nome"));
                cliente.setEndereco(this.res.getString("endereco"));
                cliente.setTelefone(this.res.getString("telefone"));
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return cliente;          
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + this.getTabela() + " (nome, endereco, telefone) VALUE (?, ?, ?)";
    }

    public void insert(Cliente cliente) {
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getInsertQuery());
            this.prSt.setString(1, cliente.getNome());
            this.prSt.setString(2, cliente.getEndereco());
            this.prSt.setString(3, cliente.getTelefone());
            this.prSt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE " + this.getTabela() + " SET nome=?, endereco=?, telefone=? WHERE id=?";
    }

    public final void update(Cliente cliente) {
        try {
            this.con=ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getUpdateQuery());
            this.prSt.setString(1, cliente.getNome());
            this.prSt.setString(2, cliente.getEndereco());
            this.prSt.setString(3, cliente.getTelefone());
            this.prSt.setInt(4, cliente.getId());
            this.prSt.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }        
    }
    
}
