/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author marcelo
 */
public class ItemOperacao {
    Item item;
    private int quantidade;
    /**
     *
     * @param item
     * @param quantidade
     */
    public ItemOperacao(Item item, int quantidade) {
        this.item = item;
        this.quantidade = quantidade;
    }
    public Item getItem() {
        return item;
    }
    public void setItem(Item item) {
        this.item = item;
    }
    public int getQuantidade() {
        return quantidade;
    }
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
