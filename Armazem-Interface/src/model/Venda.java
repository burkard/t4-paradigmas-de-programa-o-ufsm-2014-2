/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author marcelo
 */
public final class Venda extends Operacao {
    private int OperacaoId;
    private int ClienteId;
    
    /**
     *
     * @param clienteId
     * @param itens
     * @throws java.lang.Exception
     */
    public Venda(int clienteId, ArrayList<ItemOperacao> itens) throws Exception
    {
        this.setClienteId(clienteId);
        this.setItens(itens);
        if (this.checarEstoque()) {
            this.calcularValorTotal();
            this.atualizarEstoque();
        } else {
            throw new Exception("Compra inválida, estoques inferiores a quantidade desejada.");
        }
    }

    public Venda() {
        
    }

    public boolean checarEstoque() {
        ItemDAO itemDAO = new ItemDAO();
        for (ItemOperacao iten : this.getItens()) {
            Item item = itemDAO.findOne(iten.getItem().getId());
            // Se o estoque de um item for inferior a quantidade comprada retorna falso
            if (item.getEstoque() < iten.getQuantidade())
                return false;
        }
        return true;
    }
    void atualizarEstoque() {
        ItemDAO itemDAO = new ItemDAO();
        for (ItemOperacao iten : this.getItens()) {
            Item item = itemDAO.findOne(iten.getItem().getId());
            item.setEstoque(item.getEstoque() - iten.getQuantidade());
            itemDAO.update(item);
        }
    }
    public void calcularValorTotal() {
        double valor_total = 0;
        for (ItemOperacao iten : this.getItens()) {
            valor_total += (iten.getItem().getValor() * iten.getQuantidade());
        }
        this.setValorTotal(valor_total);
    }
    public int getClienteId() {
        return ClienteId;
    }
    public void setClienteId(int ClienteId) {
        this.ClienteId = ClienteId;
    }
    public int getOperacaoId() {
        return OperacaoId;
    }
    public void setOperacaoId(int OperacaoId) {
        this.OperacaoId = OperacaoId;
    }
}
