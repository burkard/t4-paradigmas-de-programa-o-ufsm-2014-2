/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author marcelo
 */
public class ClienteTable extends AbstractTable {

    private static final String[] columnNames = {"#", "Nome", "Endereço", "Telefone"};

    private final ArrayList<Cliente> records;
    
    public ClienteTable() {
        ClienteDAO clienteDAO = new ClienteDAO();
        records = clienteDAO.select();
    }
    
    public void remove(int index) {
        // Remove do BD
        ClienteDAO clienteDAO = new ClienteDAO();
        Cliente cliente = this.select(index);
        clienteDAO.delete(cliente.getId());
        // Remove da View
	records.remove(index);
	fireTableRowsDeleted(index, index);
    }
    
    public Cliente select(int index) {
        return records.get(index);
    }
    
    public void add(Cliente cliente) throws SQLException {
        // Adiciona um elemento no BD
        ClienteDAO clienteDAO = new ClienteDAO();
        clienteDAO.insert(cliente);
        // Atribui o ID do auto-incremento do bd ao objeto
        cliente.setId(clienteDAO.getLastInsertedId());
        // Adiciona um elemento na ultima posição da lista
        records.add(cliente);
        fireTableRowsInserted(records.size()-1, records.size()-1);
    }
    
    public void update(int index, Cliente cliente) {
        // Altera entidade no BD
        ClienteDAO clienteDAO = new ClienteDAO();
        Cliente clienteAux = this.select(index);
        clienteDAO.update(clienteAux);
        cliente.setId(clienteAux.getId());
        // Altera elemento na view
        records.set(index, cliente);
        fireTableRowsUpdated(index, index);
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public String getColumnName(int index) {
        return columnNames[index];
    }        

    public String getColumnCount(int columnIndex) {
        return columnNames[columnIndex];
    }    
    
    @Override
    public int getRowCount() {
        return records.size();
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex) {
            case 0: return records.get(rowIndex).getId();
            case 1: return records.get(rowIndex).getNome();
            case 2: return records.get(rowIndex).getEndereco();
            case 3: return records.get(rowIndex).getTelefone();
        }
        return null;
    }
    
}
