/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import helper.ConexaoBD;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author marcelo
 */
public class CompraDAO extends OperacaoDAO {
    
    public CompraDAO() {
        this.setTabela("compra");
    }
    
   @Override
    public ArrayList select() {
        ArrayList<Compra> compras = new ArrayList();
        Compra compra = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getSelectQuery());
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                compra = new Compra();
                compra.setId(this.res.getInt("id"));
                compra.setValorTotal(this.res.getDouble("valor_total"));
                // Carrega os itens da compra
                compra.setItens(this.selectItensOperacao(compra.getId()));
                compras.add(compra);
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return compras;
    }
    
    public Compra findOne(int id) {
        Compra compra = null;
        try {
            this.con  = ConexaoBD.conectar();
            this.prSt = this.con.prepareStatement(this.getFindOneQuery());
            this.getPrSt().setInt(1, id);
            this.res  = this.prSt.executeQuery();
            while (this.res.next()) {
                compra = new Compra();
                compra.setId(this.res.getInt("id"));
                compra.setValorTotal(this.res.getDouble("valor_total"));
                // Carrega os itens da compra
                compra.setItens(this.selectItensOperacao(compra.getId()));
            }
        } catch(Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.con, this.prSt, this.res);
        }
        return compra;
    }

    @Override
    public String getInsertQuery() {
        return "INSERT INTO " + this.getTabela() + " (valor_total) VALUE (?)";
    }

    public void insert(Compra compra) throws SQLException {
        try {
            this.setCon(ConexaoBD.conectar());
            this.setPrSt(this.getCon().prepareStatement(this.getInsertQuery()));
            this.getPrSt().setDouble(1, compra.getValorTotal());
            this.getPrSt().executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:" + e.getMessage());
        } finally {
            ConexaoBD.desconectar(this.getCon(), this.getPrSt(), this.getRes());
        }
        // Pega o ID do registro inserido
        compra.setId(this.getLastInsertedId());
        // Inserir itens da compra
        if (compra.getId() != 0) {
            this.insertItensOperacao(compra.getId(), compra.getItens());
        }
    }
    
}
