/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JOptionPane;

/**
 *
 * @author marcelo
 */
public class AbstractView extends javax.swing.JFrame  {
    
    public void showError(String msg) {
        JOptionPane.showMessageDialog(this, msg);
    }
    
}
