/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package armazem.pkginterface;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import view.MainView;

/**
 *
 * @author marcelo
 */
public class ArmazemInterface {

    /**
     * @param args the command line arguments
     * @throws java.sql.SQLException
     */
    public static void main(String[] args) throws SQLException, Exception {
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
	    public void run() {
                try {
                    new MainView().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(ArmazemInterface.class.getName()).log(Level.SEVERE, null, ex);
                }
	    }
	});

    }
    
    /**
     * http://www-usr.inf.ufsm.br/~cassales/elc117-2014b/slides/slides-java-gui-2014b.pdf
     * https://solutionbto.wordpress.com/2013/05/26/criando-classe-de-conexao-java-parte-1/
     * http://solutionbto.wordpress.com/2013/07/06/criando-classe-de-conexao-java-parte-2-mvcjdbccrud/
     */
    
}
