/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import helper.ComboItem;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cliente;
import model.ClienteDAO;
import model.Item;
import model.ItemDAO;
import model.ItemOperacao;
import model.Venda;
import model.VendaModel;
import view.VendaView;

/**
 *
 * @author marcelo
 */
public class VendaController {

    private final VendaView view;
    private final VendaModel model;
    
    public VendaController(VendaView view, VendaModel model) {
        this.view = view;
        this.model = model;
        // Adiciona clientes ao JComboBox
        ClienteDAO clienteDAO = new ClienteDAO();
        ArrayList<Cliente> clientes = clienteDAO.select();
        ComboItem[] listClientes = new ComboItem[clientes.size()];
        int i = 0;
        for (Cliente cliente : clientes) {
            listClientes[i] = new ComboItem(cliente.getId(), cliente.getNome());
            i++;
        }
        this.view.getSelectCliente().setModel(new javax.swing.DefaultComboBoxModel(listClientes));
        
        // Adiciona produtos ao JComboBox
        ItemDAO itemDAO = new ItemDAO();
        ArrayList<Item> produtos = itemDAO.select();
        ComboItem[] listProdutos = new ComboItem[produtos.size()];
        i = 0;
        for (Item produto : produtos) {
            listProdutos[i] = new ComboItem(produto.getId(), produto.getNome());
            i++;
        }
        this.view.getSelectProduto().setModel(new javax.swing.DefaultComboBoxModel(listProdutos));        
    }

    public void insert() throws Exception {
        Venda v = newFromView();
        if (v != null) {
            try {
                model.add(v);
                this.clear();
                view.showError("Venda incluída com sucesso!");
            } catch (SQLException ex) {
                Logger.getLogger(ClienteController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void clear() {
        view.getTextQuantidade().setText("");
    }    
    
    private Venda newFromView() throws Exception {
        try {
            // Pega o cliente selecionado
            ComboItem clienteSelected = (ComboItem) view.getSelectCliente().getSelectedItem();
            int clienteId = clienteSelected.getValue();
            System.out.println("Cliente ID:" + clienteId);
            // Pega o produto selecionado
            ComboItem produtoSelected = (ComboItem) view.getSelectProduto().getSelectedItem();
            int itemId = produtoSelected.getValue();
            System.out.println("Produto ID:" + itemId);
            // Pega a quantidade
            int quantidade = Integer.parseInt(view.getTextQuantidade().getText());
            // Pega o objeto do produto selecionado
            ItemDAO itemDAO = new ItemDAO();
            Item item = itemDAO.findOne(itemId);
            ItemOperacao itemOperacao = new ItemOperacao(item, quantidade);
            ArrayList<ItemOperacao> itens = new ArrayList();
            itens.add(itemOperacao);
            Venda v = new Venda(clienteId, itens);
            return v;
        } catch (Exception e) {
            view.showError(e.getMessage());
            return null;
        }
    }
    
}
