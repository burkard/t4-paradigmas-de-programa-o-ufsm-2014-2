/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import model.ItemDAO;
import view.RelatorioFinanceiroView;

/**
 *
 * @author marcelo
 */
public class RelatoriosController {
    
    public void financeiro(RelatorioFinanceiroView view, ItemDAO model) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        
        view.getLabelTotalEntradas().setText(formatter.format(model.getTotalEntradas()));
        view.getLabelTotalSaidas().setText(formatter.format(model.getTotalSaidas()));
        double saldo = model.getSaldo();
        view.getLabelSaldo().setText(formatter.format(saldo) + ((saldo >= 0) ? " de Lucro" : " de Prejuízo"));
    }
    
}
