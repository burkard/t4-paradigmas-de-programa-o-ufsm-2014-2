/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cliente;
import model.ClienteTable;
import view.ClienteView;

/**
 *
 * @author marcelo
 */
public class ClienteController extends AbstractController {
    
    private final ClienteView view;
    private ClienteTable model;
    
    public ClienteController(ClienteView view, ClienteTable model) {
        this.view = view;
        this.model = model;
    }
    
    @Override
    public void insert() {
        Cliente c = newFromView();
        if (c != null) {
            try {
                model.add(c);
            } catch (SQLException ex) {
                Logger.getLogger(ClienteController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void update() {
        int index = view.getTableClientes().getSelectedRow();
        if (index == -1) {
            return;
        }
        Cliente c = newFromView();
        if (c != null) {
            model.update(index, c);
        }
    }

    @Override
    public void remove() {
        int index = view.getTableClientes().getSelectedRow();
        if (index == -1) {
            return;
        }
        model.remove(index);
    }

    @Override
    public void select() {
        int index = view.getTableClientes().getSelectedRow();
        if (index == -1) {
            return;
        }
        Cliente c = model.select(index);
        view.getTextNome().setText(c.getNome());
        view.getTextEndereco().setText(c.getEndereco());
        view.getTextTelefone().setText(c.getTelefone());
    }

    @Override
    public void clear() {
        view.getTextNome().setText("");
        view.getTextEndereco().setText("");
        view.getTextTelefone().setText("");
    }
    
    public void refresh() {
        model = new ClienteTable();
        view.getTableClientes().setModel(model);
    }

    private Cliente newFromView() {
        try {
            Cliente c = new Cliente();
            c.setNome(view.getTextNome().getText());
            c.setEndereco(view.getTextEndereco().getText());
            c.setTelefone(view.getTextTelefone().getText());
            return c;
        } catch (NumberFormatException e) {
            view.showError("Dado(s) de entrada invalido(s)!");
            return null;
        }
    }
    
}
