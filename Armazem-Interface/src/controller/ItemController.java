/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Item;
import model.ItemTable;
import view.ItemView;

/**
 *
 * @author marcelo
 */
public class ItemController extends AbstractController {
    
    private final ItemView view;
    private ItemTable model;
    
    public ItemController(ItemView view, ItemTable model) {
        this.view = view;
        this.model = model;
    }
    
    @Override
    public void insert() {
        Item i = newFromView();
        if (i != null) {
            try {
                model.add(i);
            } catch (SQLException ex) {
                Logger.getLogger(ClienteController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void update() {
        int index = view.getTableProdutos().getSelectedRow();
        if (index == -1) {
            return;
        }
        Item i = newFromView();
        if (i != null) {
            model.update(index, i);
        }
    }

    @Override
    public void remove() {
        int index = view.getTableProdutos().getSelectedRow();
        if (index == -1) {
            return;
        }
        model.remove(index);
    }

    @Override
    public void select() {
        int index = view.getTableProdutos().getSelectedRow();
        if (index == -1) {
            return;
        }
        Item i = model.select(index);
        view.getTextNome().setText(i.getNome());
        view.getTextValor().setText(Double.toString(i.getValor()));
    }

    @Override
    public void clear() {
        view.getTextNome().setText("");
        view.getTextValor().setText("");
    }
    
    public void refresh() {
        model = new ItemTable();
        view.getTableProdutos().setModel(model);        
    }

    private Item newFromView() {
        try {
            Item i = new Item();
            i.setNome(view.getTextNome().getText());
            i.setValor(Double.parseDouble(view.getTextValor().getText()));
            return i;
        } catch (NumberFormatException e) {
            view.showError("Dado(s) de entrada invalido(s)!");
            return null;
        }
    }    
    
}
