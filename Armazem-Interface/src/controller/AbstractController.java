/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author marcelo
 */
public abstract class AbstractController {
    public abstract void insert();
    public abstract void update();
    public abstract void remove();
    public abstract void select();
    public abstract void clear();
}
