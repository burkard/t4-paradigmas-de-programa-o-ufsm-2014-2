/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.ItemDAO;
import model.RelatorioVendaItem;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import view.GraficoVendaProdutoView;

/**
 *
 * @author marcelo
 */
public class GraficoProdutoController {
    
    private GraficoVendaProdutoView view;
    private ItemDAO model;

    public GraficoProdutoController(GraficoVendaProdutoView view, ItemDAO model) {
        this.view = view;
        this.model = model;
    }
    
    public void showPieChart() {
        JFreeChart chart = ChartFactory.createPieChart(null, this.createDataset(), true, true, true);
        PiePlot p = (PiePlot) chart.getPlot();
        ChartPanel frame = new ChartPanel(chart, true);
        frame.setVisible(true);
        frame.setSize(500, 400);
        
        view.getGraficoPainel().removeAll();
        view.getGraficoPainel().add(frame);
        view.getGraficoPainel().revalidate();
        view.getGraficoPainel().repaint();
    }
    
    private DefaultPieDataset createDataset() { 
        DefaultPieDataset pieDataset = new DefaultPieDataset();
        ItemDAO itemDAO = new ItemDAO();
        ArrayList<RelatorioVendaItem> itens = itemDAO.selectVendasItem();
        for (RelatorioVendaItem iten : itens) {
            pieDataset.setValue(iten.getItem().getNome(), iten.getQuantidade());
        }
        return pieDataset;
    }    
    
}
