/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.ItemDAO;
import model.RelatorioVendaCliente;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import view.GraficoVendaClienteView;

/**
 *
 * @author marcelo
 */
public class GraficoClienteController {
    
    private GraficoVendaClienteView view;
    private ItemDAO model;

    public GraficoClienteController(GraficoVendaClienteView view, ItemDAO model) {
        this.view = view;
        this.model = model;
    }
    
    public void showPieChart() {
        JFreeChart chart = ChartFactory.createPieChart(null, this.createDataset(), true, true, true);
        PiePlot p = (PiePlot) chart.getPlot();
        ChartPanel frame = new ChartPanel(chart, true);
        frame.setVisible(true);
        frame.setSize(500, 400);
        
        view.getGraficoPainel().removeAll();
        view.getGraficoPainel().add(frame);
        view.getGraficoPainel().revalidate();
        view.getGraficoPainel().repaint();
    }
    
    private DefaultPieDataset createDataset() { 
        DefaultPieDataset pieDataset = new DefaultPieDataset();
        ItemDAO itemDAO = new ItemDAO();
        int totalItensVendidos = itemDAO.sumVendasCliente();
        int itensVendidosTop5  = 0;
        ArrayList<RelatorioVendaCliente> itens = itemDAO.selectVendasClienteTop5();
        for (RelatorioVendaCliente iten : itens) {
            pieDataset.setValue(iten.getCliente().getNome(), iten.getQuantidade());
            itensVendidosTop5 += iten.getQuantidade();
        }
        // Se houver outros além de 5, agrupa no somatório
        if (totalItensVendidos > itensVendidosTop5) {
            pieDataset.setValue("Outros", (totalItensVendidos - itensVendidosTop5));
        }
        return pieDataset;
    }    
    
}
