/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import helper.ComboItem;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Compra;
import model.CompraModel;
import model.Item;
import model.ItemDAO;
import model.ItemOperacao;
import view.CompraView;

/**
 *
 * @author marcelo
 */
public class CompraController {
    
    private final CompraView view;
    private final CompraModel model;
    
    public CompraController(CompraView view, CompraModel model) {
        this.view = view;
        this.model = model;
        // Adiciona produtos ao JComboBox
        ItemDAO itemDAO = new ItemDAO();
        ArrayList<Item> produtos = itemDAO.select();
        ComboItem[] listProdutos = new ComboItem[produtos.size()];
        int i = 0;
        for (Item produto : produtos) {
            listProdutos[i] = new ComboItem(produto.getId(), produto.getNome());
            i++;
        }
        this.view.getSelectProduto().setModel(new javax.swing.DefaultComboBoxModel(listProdutos));        
    }    
    
    public void insert() throws Exception {
        Compra c = newFromView();
        if (c != null) {
            try {
                model.add(c);
                this.clear();
                view.showError("Compra incluída com sucesso!");
            } catch (SQLException ex) {
                Logger.getLogger(ClienteController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void clear() {
        view.getTextQuantidade().setText("");
        view.getTextValorTotal().setText("");
    }    
    
    private Compra newFromView() throws Exception {
        try {
            // Pega o produto selecionado
            ComboItem produtoSelected = (ComboItem) view.getSelectProduto().getSelectedItem();
            int itemId = produtoSelected.getValue();
            System.out.println("Produto ID:" + itemId);
            // Pega o valor total da compra
            double valorTotal = Double.parseDouble(view.getTextValorTotal().getText());
            // Pega a quantidade
            int quantidade = Integer.parseInt(view.getTextQuantidade().getText());
            // Pega o objeto do produto selecionado
            ItemDAO itemDAO = new ItemDAO();
            Item item = itemDAO.findOne(itemId);
            ItemOperacao itemOperacao = new ItemOperacao(item, quantidade);
            ArrayList<ItemOperacao> itens = new ArrayList();
            itens.add(itemOperacao);
            Compra c = new Compra(itens, valorTotal);
            return c;
        } catch (Exception e) {
            view.showError(e.getMessage());
            return null;
        }
    }
    
}
